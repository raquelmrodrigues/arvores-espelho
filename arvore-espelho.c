#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
   char info;
   struct arvore *esq;
   struct arvore *dir;
} Arvore;


Arvore*  cria_arv_vazia (void);
Arvore*  arv_constroi (char c, Arvore* e, Arvore* d);
int      verifica_arv_vazia (Arvore* a);
Arvore*  arv_libera (Arvore* a);
int      arv_pertence (Arvore* a, char c);
void     arv_imprime (Arvore* a);

Arvore* cria_arv_vazia (void) {
   return NULL;
}

Arvore* arv_constroi (char c, Arvore* e, Arvore* d) {
  Arvore* a = (Arvore*)malloc(sizeof(Arvore));
  a->info = c;
  a->esq = e;
  a->dir = d;
  return a;
}

int verifica_arv_vazia (Arvore* a) {
  return (a == NULL);
}

Arvore* arv_libera (Arvore* a) {
  if (!verifica_arv_vazia(a)) {
    arv_libera (a->esq);
    arv_libera (a->dir);
    free(a);
  }
  return NULL;
}

int eh_espelho(Arvore * arv_a, Arvore * arv_b) {
  
  if (arv_a == NULL && arv_b == NULL) {
    return 1;
  }
  if ((arv_a == NULL || arv_b == NULL) || arv_a->info != arv_b->info) {
    return 0;
  }
  else {

    int a = eh_espelho(arv_a->esq, arv_b->dir);
    int b = eh_espelho(arv_a->dir, arv_b->esq);

    if (a == 1 && b == 1) {
      return 1;
    }
    else {
      return 0;
    }
  }
}

struct Arvore* clonarArvore(Arvore *root){
  if(root == NULL)  
    return NULL;

  Arvore* novaArvore = arv_constroi(root->info, cria_arv_vazia(), cria_arv_vazia());

  novaArvore->esq = clonarArvore(root->esq);
  novaArvore->dir = clonarArvore(root->dir);
  return novaArvore;
}

Arvore * cria_espelho(Arvore * arv_a) {
  Arvore *arv_b = clonarArvore(arv_a);

  if (arv_b == NULL) {
    return;
  } 
  else {
    cria_espelho(arv_b->esq);
    cria_espelho(arv_b->dir);

    Arvore * temp = arv_b->esq;
    arv_b->esq = arv_b->dir;
    arv_b->dir = temp;

    return arv_b;
  }
}

int main (int argc, char *argv[]) {
  // Arvore *aa, *ad, *ab, *ae, *af, *ac;
  // ad = arv_constroi('d',cria_arv_vazia(),cria_arv_vazia());
  // ab = arv_constroi('b',cria_arv_vazia(),ad);
  // ae = arv_constroi('e',cria_arv_vazia(),cria_arv_vazia());
  // af = arv_constroi('f',cria_arv_vazia(),cria_arv_vazia());
  // ac = arv_constroi('c',ae,af);
  // aa  = arv_constroi('a',ab,ac);

  // Arvore *ba, *bd, *bb, *be, *bf, *bc;
  // bd = arv_constroi('d',cria_arv_vazia(),cria_arv_vazia());
  // bb = arv_constroi('b', bd, cria_arv_vazia());
  // be = arv_constroi('e',cria_arv_vazia(),cria_arv_vazia());
  // bf = arv_constroi('f',cria_arv_vazia(),cria_arv_vazia());
  // bc = arv_constroi('c',bf,be);
  // ba  = arv_constroi('a',bc,bb);

  Arvore *a1, *b1, *c1;
  b1 = arv_constroi('b', cria_arv_vazia(), cria_arv_vazia());
  c1 = arv_constroi('c', cria_arv_vazia(), cria_arv_vazia());
  a1 = arv_constroi('a', b1, c1);

  // Arvore *a2, *b2, *c2;
  // c2 = arv_constroi('c', cria_arv_vazia(), cria_arv_vazia());
  // b2 = arv_constroi('b', cria_arv_vazia(), cria_arv_vazia());
  // a2 = arv_constroi('a', c2, b2);

  // printf("%i", eh_espelho(aa, ba));

  Arvore* arvoreEspelho = cria_espelho(a1);

  printf("%i", eh_espelho(a1, arvoreEspelho));
}



